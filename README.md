# Lumu Backend Integrations Test

## About

This project consisted on making a python script with two main functionalities. The first one was about sending a DNS
query array in
chunks to the **Lumu API** by the endpoint **_Send DNS Queries_**. The second one is about calculating and printing the
stats of the parsed data.

## How to run locally

1. Clone the repository\
   `git clone git@github.com:lufvelasquezgo/lumu-integrations-test.git`
2. Open the project on your favorite editor.
3. Set up a virtual environment in order to install the requirements.\
   `pip install -r requirements.txt`
4. Open the terminal (keep in mind to be on the main directory lumu-integrations-test) and run the following command\
   `python main.py --help`\
   <img src="images/cli.png" alt="Command Line Interface options" width="500"/>\
   As you may see, you must give a file to be parsed as an argument,the one that we are going to use is the following
   `query_samples/queries`. In the end, the command that you are going to run is `python main.py query_samples/queries`.
5. The result after running the command is going to be the print of the stats.\
   <img src="images/first_response.png" alt="First response" width="180"/>\
   <img src="images/second_response.png" alt="Second response" width="400"/>

## Computational Complexity

We generally use the Big-O notation to describe the time complexity. When using the Big-O notation, we describe the
algorithm's efficiency based on the increasing size of the input data (n).

### Analyzing each method

<img src="images/method_collect_from_file.png" width="400"/>\
In this method the first complexity is with the `open()` function which has a constant complexity **O(1)**.\
The second complexity is when we want to go over into each line of the file. We do this with a `for function` which has a linear complexity **O(n)**.\
The third complexity that we will see is with the method `add_query` which has a constant complexity **O(1)** because of its append method\
<img src="images/method_add_query.png" width="400"/>\
After it, we see a fourth constant complexity **O(1)** with the `if function` and constant complexity **O(1)** with the method `count_queries`.\
Which means that the total complexity of this method will be a **O(n)**

Furthermore, for the second method we have a \
<img src="images/print_stats.png" alt="First response" width="400"/>\
which uses the builtin method `sorted`, which is **O(nlogn)**. However, the worst case would be when all the IPs and Hosts are different, e.g. `n` would be the number of lines in the queries file.
In the average case, the `sorted` method will be applied to a smaller number of items.

# Final notes

We can conclude that my approach is _lazy_ and is not a memory-hog.
This is because we are not loading all the file in memory but iterating over each line.
Also, we are using chunks in order to send the data in batches to the API, avoiding to overload the network with huge bodies.
Also, my approach lets extend the behavior, like adding other statistics quantities, because of the `DNSQueriesCollector` class.
The OOP approach gives us some advantages, like splitting responsibilities.
Furthermore, based on my implementation, we could implement unit testing easily.
However, we did not implement that due to lack of time (and it was out of the test scope).
Finally, I would like to say that storing secrets and tokens in .env files is not a good practice but for testing purpose, I let them in the repository.

