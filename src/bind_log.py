from __future__ import annotations
from typing import Dict
from datetime import datetime


class BindLog:
    """
    The BindLog object contains variables from a bind9 query log
    :param timestamp: The UTC date and time when query request was performed in ISO format
    :param host: The domain name being queried
    :param ip: The dotted-quad formatted or colon-separated IPv4/IPv6 address of the client that initiated the request
    :param query_type: The type of data contained in this resource record
    """

    def __init__(self, timestamp: datetime, host: str, ip: str, query_type: str):
        self.timestamp = timestamp
        self.host = host
        self.ip = ip
        self.query_type = query_type

    @classmethod
    def from_text(cls, line: str) -> BindLog:
        """
        It set the instance attributes needed for the query
        :param line: the bind9 query log line
        :return: instance of a BindLog
        """
        line_items = line.split()
        date = line_items[0]
        time = line_items[1]
        timestamp = datetime.strptime(f"{date} {time}", "%d-%b-%Y %H:%M:%S.%f")
        host = line_items[9]
        ip, _ = line_items[6].split("#")
        query_type = line_items[11]
        return cls(timestamp, host, ip, query_type)

    def get_dns_query_body(self) -> Dict:
        """
        It's a method to parse the query data to a Dict
        :return: the variables parsed
        """
        return {
            "timestamp": self.timestamp.isoformat()[:-3] + "Z",
            "name": self.host,
            "client_ip": self.ip,
            "type": self.query_type,
        }
