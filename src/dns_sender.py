import os
from dotenv import load_dotenv
from src.bind_log import BindLog

import requests

load_dotenv()

COLLECTION_ID = os.getenv("COLLECTION_ID")
LUMU_CLIENT_KEY = os.getenv("LUMU_CLIENT_KEY")


class DNSSender:
    """
    The DNSSender object contains methods to create the queries, send those queries, and count them
    """

    def __init__(self):
        self.queries = []

    def add_query(self, bind_log: BindLog) -> None:
        """
        It is a method to construct the DNS queries array from the bind log
        :param bind_log: It is an instance of the BindLog object
        """
        query = bind_log.get_dns_query_body()
        self.queries.append(query)

    def send_queries(self) -> None:
        """
        It is a method to send the DNS queries array to the Lumu API
        """
        url = f"https://api.lumu.io/collectors/{COLLECTION_ID}/dns/queries"
        params = {"key": LUMU_CLIENT_KEY}
        json_body = self.queries
        response = requests.post(url, params=params, json=json_body)

        if response.status_code != 200:
            print("Some DNS queries were not sent to Lumu.")

        self.queries = []

    def count_queries(self) -> int:
        """
        It is a method to count the DNS queries array
        :return: the amount of queries in the DNS queries array
        """
        return len(self.queries)
