from collections import defaultdict
from typing import Dict

from tabulate import tabulate

from src.bind_log import BindLog
from src.dns_sender import DNSSender


class DNSQueriesCollector:
    """
    The DNSQueriesCollector compute the rank and stats from a bind9 query log file
    """

    def __init__(self):
        self.ip_count = defaultdict(int)
        self.host_count = defaultdict(int)
        self.total_records = 0

    def collect_from_file(self, chunk_size: int, filename: str) -> None:
        """
        It collects the IPs, Hosts, the number of time each one of this repeat, and the total records
        :param chunk_size: The number of records
        :param filename: the file with the bind9 query logs
        """
        with open(filename) as file:
            dns_server = DNSSender()
            for line in file:
                bind_log = BindLog.from_text(line)

                self.ip_count[bind_log.ip] += 1
                self.host_count[bind_log.host] += 1
                self.total_records += 1

                dns_server.add_query(bind_log)
                if dns_server.count_queries() == chunk_size:
                    print("Sending queries to Lumu ...")
                    dns_server.send_queries()

            if dns_server.count_queries() > 0:
                dns_server.send_queries()

    def print_stats(self, rank: int) -> None:
        """
        It is a method to print the statistics of the IPs and Hosts rank in console
        :param rank: The number of positions that we want to return
        """
        head_ip_count = self.get_n_highest_values(self.ip_count, rank)
        head_host_count = self.get_n_highest_values(self.host_count, rank)

        head_ip_rows_with_percentage = [
            (ip, count, self.compute_percentage_as_string(count, self.total_records))
            for ip, count in head_ip_count.items()
        ]

        head_host_rows_with_percentage = [
            (host, count, self.compute_percentage_as_string(count, self.total_records))
            for host, count in head_host_count.items()
        ]
        headers_ip_rows = ["Client IPs Rank", "Count", "Percentage"]
        headers_host_rows = ["Hosts Rank", "Count", "Percentage"]
        print(f"Total records {self.total_records}")
        print()
        print(tabulate(head_ip_rows_with_percentage, headers_ip_rows))
        print()
        print(tabulate(head_host_rows_with_percentage, headers_host_rows))

    @staticmethod
    def get_n_highest_values(dict_values: Dict, n: int) -> Dict:
        """
        It is a static method to get N the highest values
        :param dict_values: dictionary with values
        :param n: the number of items to return
        :return: A sorted dictionary with the n values
        """
        sorted_by_value = sorted(
            dict_values.items(), key=lambda item: item[1], reverse=True
        )
        head = sorted_by_value[:n]
        return {key: value for key, value in head}

    @staticmethod
    def compute_percentage_as_string(count: int, total: int) -> str:
        """
        It is a static method to compute the percentage some values represent
        :param count: time that each value repeats
        :param total: total amount of values
        :return: a string percentage with just two decimal numbers
        """
        percentage = count * 100 / total
        return f"{percentage:.2f}%"
