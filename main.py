from src.dns_queries_collector import DNSQueriesCollector

import click


@click.command()
@click.option(
    "--rank", default=5, help="The number of positions that we want to return."
)
@click.option(
    "--chunk_size",
    default=500,
    help="The number of records that we want to send to Lumu at the same time.",
)
@click.argument("file", type=click.Path(exists=True))
def main(rank: int, chunk_size: int, file: str):
    dns_queries_collector = DNSQueriesCollector()
    dns_queries_collector.collect_from_file(chunk_size, file)
    dns_queries_collector.print_stats(rank)


if __name__ == "__main__":
    main()
